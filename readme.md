# Choo-choo Train

* * *

## What is it?

* This is an automated model of railway track between the stations "Minsk Sea" and "Ratomka".

* In near future it will look like:

![](https://bytebucket.org/iivanov550503/choo-choo-train/raw/d827b77662adceee673aac121dc96159032bb3b2/docs/models/model.jpg)

## What is this for?

* Created in entertainment purposes only.

## Requirements

* Good mood
* Some friends