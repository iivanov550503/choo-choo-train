#include "PCF8574.h"

// https://github.com/RobTillaart/Arduino/tree/master/libraries/PCF8574

PCF8574 PCF(0x3E);

void setup() {
  Serial.begin(115200);
  PCF.begin();
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  digitalWrite(LED_BUILTIN, PCF.read(0));
  PCF.write(1, HIGH);
  delay(300);
  PCF.write(1, LOW);
  delay(300);
}
