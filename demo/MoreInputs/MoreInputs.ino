#include <Arduino.h>
#include <MoreInputs.h>
using namespace LibMorePins;
/**
 * MorePins Library usage example
 * 
 * Scenario:
 * Run a function whenever a button is pressed.
 * 
 * This example will print a message on the serial monitor
 * each time the button is pressed.
 */


// Instantiate a MoreInputs object.
// We're using 1 SN54HC165 IC
// We're using Arduino pins 5, 6 and 7.
// 5 for SH/LD
// 6 for CLK
// 7 for QH (Data)
MoreInputs mi = MoreInputs(1, MoreInputs::PINS_UNO_5_6_7);

void setup() {
  Serial.begin(9600);
}

void loop() {
  mi.readData();
  auto pins = mi.pins;
  for (int i = 0; i < pins.size(); i++){
      Serial.println(pins[i]);
   }
   delay(1000);
}

