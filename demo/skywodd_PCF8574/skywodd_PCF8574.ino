#include "PCF8574.h"

// https://github.com/skywodd/pcf8574_arduino_library/tree/master/PCF8574

PCF8574 pcf8574;

void setup()
{
  pcf8574.begin(0x3F);
  pinMode(LED_BUILTIN, OUTPUT);
	pcf8574.pinMode(1, OUTPUT);
	pcf8574.pinMode(2, INPUT);
}

void loop()
{
  digitalWrite(LED_BUILTIN, pcf8574.digitalRead(2));
	pcf8574.digitalWrite(1, HIGH);
	delay(300);
	pcf8574.digitalWrite(1, LOW);
	delay(300);
}
