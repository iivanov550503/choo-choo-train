#include "PCF8574.h"

// https://github.com/xreef/PCF8574_library

PCF8574 pcf8574(0x3F);

void setup()
{
  pinMode(LED_BUILTIN, OUTPUT);
	pcf8574.pinMode(P1, OUTPUT);
	pcf8574.pinMode(P2, INPUT);
	pcf8574.begin();
  pcf8574.write(LOW);
}

void loop()
{
  digitalWrite(LED_BUILTIN, pcf8574.digitalRead(P2));
	pcf8574.digitalWrite(P1, HIGH);
	delay(300);
	pcf8574.digitalWrite(P1, LOW);
	delay(300);
}
